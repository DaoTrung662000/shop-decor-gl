<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Dao Viet Trung",
            'email' =>'viettrung662000@gmail.com',
            'password' => Hash::make(12345678),
            'level' => 2
        ]);

        DB::table('users')->insert([
            'name' => "Dao Viet Trung khach hang",
            'email' =>'viettrungkh@gmail.com',
            'password' => Hash::make(12345678),
            'level' => 0
        ]);
    }
}
